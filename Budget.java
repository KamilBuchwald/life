import org.json.JSONException;

class Budget {


    private static double monthlyIncomingNetto;
    private double salaryNetto, monthlyStaticCosts, monthlyBudget, currentSavings;
    private double savings, travels, expenses;


    public Budget(double SN) {


        salaryNetto = SN;


    }

    public void setMonthlyStaticCosts(double costs) {

        monthlyStaticCosts = costs;


    }

    public void calculateBudget() throws JSONException {


        Data data = new Data();
        monthlyStaticCosts = data.getStaticCosts();
        monthlyBudget = salaryNetto - monthlyStaticCosts;
        monthlyIncomingNetto = salaryNetto - monthlyStaticCosts;

        expenses = monthlyBudget * 0.1538;
        travels = monthlyBudget * 0.2570;
        savings = monthlyBudget - expenses - travels;

        if (expenses <= 250) {
            monthlyBudget = monthlyBudget - 250.00;
            expenses = 250.00;
            travels = monthlyBudget * 0.10538;
            savings = monthlyBudget - travels;
        }
        if (monthlyBudget < 0) {
            expenses = 0;
            travels = 0;
            savings = 0;
        }


    }

    public double getSavings() {
        return savings;
    }

    public double getTravels() {
        return travels;
    }

    public double getExpenses() {
        return expenses;
    }

    public double getSalary() {
        return salaryNetto;
    }

    public double getMonthlyIncomingNetto() {
        return monthlyIncomingNetto;
    }


    public void getCurrentInformations() throws JSONException {

        Data dat = new Data();
        dat.getInformation();

    }

    public void saveInformations() {
        //Zapisanie danych do pliku JSON/txt
    }

}
