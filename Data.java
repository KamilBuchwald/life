
import java.io.*;

import org.json.*;
import org.json.JSONException;

public class Data {


    private String dataraw;
    protected double currentSavings, currentTravels, staticCosts;
    protected JSONArray travelsArray, goalsArray, revenuesArray;
    protected JSONObject JSONSource;


    public Data() throws JSONException {
        this.getInformation();


    }

    public void getInformation() throws JSONException {

        String result = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader("/home/devidea/Projects/workspace/Life/src/informations.json"));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
            dataraw = result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jobj = new JSONObject(dataraw);
        JSONSource = jobj;
        currentSavings = jobj.getDouble("Savings");
        currentTravels = jobj.getDouble("Travels");


        JSONArray jarr = jobj.getJSONArray("staticCosts");

        double suma = 0;

        for (int i = 0; i < jarr.length(); i++) {
            suma = suma + jarr.getDouble(i);

        }
        staticCosts = suma;
        travelsArray = jobj.getJSONArray("holidaysGoals");
        goalsArray = jobj.getJSONArray("Goals");
        revenuesArray = jobj.getJSONArray("Revenues");


    }


    public double getCurrentSavings() {
        return currentSavings;
    }

    public double getCurrentTravels() {
        return currentTravels;
    }

    public double getStaticCosts() {
        return staticCosts;
    }


}
