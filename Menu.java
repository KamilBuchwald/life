import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Menu {

    public void Menu() {

    }

    public void showMenu() throws JSONException, IOException {

        System.out.println(" ________________________________");
        System.out.println("|__________Life Machine__________|");
        System.out.println("|                                |");
        System.out.println("|1.Oblicz miesięczny budżet      |");
        System.out.println("|2.Pokaż raport całkowity        |");
        System.out.println("|3.Opcje                         |");
        System.out.println("|________________________________|");
        this.selectOption();


    }

    public void selectOption() throws JSONException, IOException {

        Scanner sc = new Scanner(System.in);
        String select = sc.next();

        if (select.equals("1")) {
            this.showBudget();

        } else if (select.equals("2")) {
            this.showReport();


        } else if (select.equals("3")) {
            this.options();

        } else {
            System.out.println("Opcja nie istnieje");
            this.showMenu();
        }


    }


    private void showBudget() throws JSONException, IOException {

        System.out.println("Podaj miesięczną kwotę netto");
        Scanner sc;
        int decision;
        sc = new Scanner(System.in);
        Budget budget = new Budget(sc.nextInt());
        budget.calculateBudget();
        System.out.println("Miesięczy budżet przy zarobku: " + budget.getSalary() + " netto wynosi: " + budget.getMonthlyIncomingNetto());
        System.out.println("Wydatki własne: " + budget.getExpenses());
        System.out.println("Podróże: " + budget.getTravels());
        System.out.println("Oszczedności: " + budget.getSavings());
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
        Data data = new Data();
        data.getInformation();
        System.out.println("Aktualne oszczędności wynoszą: " + data.getCurrentSavings() + " zł" + " Po wpłacie nowych środków: " + (data.getCurrentSavings() + budget.getSavings()) + "zł");
        System.out.println("Aktualny Budżet podróżniczy wynosi : " + data.getCurrentTravels() + " zł" + " Po wpłacie nowych środków: " + (data.getCurrentTravels() + budget.getTravels()) + "zł");
        System.out.println("Czy zapisać dane do bazy? Jeśli tak wybierz 1 jeśli nie wybierz 0");
        decision = sc.nextInt();
        if(decision == 1){
            JSONObject tempJOBJECT = new JSONObject();
            JSONArray tempJARRAY = new JSONArray();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            System.out.println();


            tempJOBJECT.put("date",dtf.format(now));
            tempJOBJECT.put("travels",budget.getTravels());
            tempJOBJECT.put("savings",budget.getSavings());
            tempJOBJECT.put("salary", budget.getSalary());
            tempJOBJECT.put("budget", (budget.getSalary() - data.staticCosts));
            tempJOBJECT.put("expenses", budget.getExpenses());

            Save save = new Save();
            save.saveRevenues(tempJOBJECT);



        }else if(decision == 0){
            System.out.print("\033[H\033[2J");
            System.out.flush();
            this.showMenu();

        }

        System.out.print("\033[H\033[2J");
        System.out.flush();
        this.showMenu();

    }

    private void showReport() throws JSONException, IOException {

        Report report = new Report();

        JSONObject travels, goals;

        goals = report.recieveGoals();
        System.out.print("\033[H\033[2J");
        System.out.flush();


        System.out.println("________________________________________________________________________________");
        System.out.println("________________________________GŁÓWNE CELE_____________________________________");

        System.out.println("  ");
        System.out.println("  ");

        JSONArray jAr = new JSONArray();
        jAr = goals.getJSONArray("Cele");
        JSONObject jr = new JSONObject();

        for (int i = 0; i < jAr.length(); i++) {
            System.out.println("------------------------------ CEL" + (i + 1) + "----------------------------------");
            jAr.getJSONObject(i);
            jr = jAr.getJSONObject(i);
            System.out.println("  ");
            System.out.println("  ");
            System.out.println("Cel: " + jr.getString("Cel"));
            System.out.println("Koszt całkowity: " + jr.getDouble("Koszt całkowity"));
            System.out.println("Procent dostępności: " + jr.getInt("Procent dostępności") + " %");
            System.out.println("Priorytet: " + jr.getInt("Priorytet"));

        }


        System.out.println("________________________________________________________________________________");
        System.out.println("____________________________________PODRÓŻE_____________________________________");
        System.out.println("  ");
        System.out.println("  ");

        travels = report.recieveTravels();

        jAr = travels.getJSONArray("Holidays");


        for (int i = 0; i < jAr.length(); i++) {
            System.out.println("  ");
            System.out.println("  ");
            System.out.println("------------------------------ PODRÓŻ" + (i + 1) + "----------------------------------");
            jAr.getJSONObject(i);
            jr = jAr.getJSONObject(i);

            System.out.println("Cel: " + jr.getString("Nazwa"));
            System.out.println("Koszt całkowity: " + jr.getDouble("Koszt całkowity"));
            System.out.println("Procent dostępności: " + jr.getInt("Procent dostępności") + " %");
            String tm_dec;
            if (jr.getBoolean("Dostępność") == true) {
                tm_dec = "Dostępne";
            } else {
                tm_dec = "Niedostępne";
            }
            System.out.println("Dostępność: " + tm_dec);

        }


        System.out.println("  ");
        System.out.println("  ");
        System.out.println("Nacinij dowolny przycisk aby kontynuowac ");
        System.out.println("  ");
        Scanner sc = new Scanner(System.in);
        sc.next();
        System.out.print("\033[H\033[2J");
        System.out.flush();
        this.showMenu();


    }

    private void options() throws JSONException, IOException {

        System.out.println(" ________________________________");
        System.out.println("|_____________Opcje______________|");
        System.out.println("|                                |");
        System.out.println("|1.Ustaw koszty stałe            |");
        System.out.println("|2.Ustaw budżet oszczędnościowy  |");
        System.out.println("|3.Ustaw budżet podróżniczy      |");
        System.out.println("|4.Opuść opcje                   |");
        System.out.println("|                                |");
        System.out.println("|________________________________|");
        System.out.println("|    Wybierz odpowiednią opcję   |");
        System.out.println("|________i nacśnij enter_________|");
        Scanner scanner = new Scanner(System.in);
        String decision;
        double dec;
        Data data = new Data();
        data.getInformation();
        decision = scanner.next();
        if(decision.equals("1")){

            System.out.println("|________________________________|");
            System.out.println("| Aktualne koszty stałe wynoszą: |");
            System.out.println("|   "+data.getStaticCosts() + "  |");
            System.out.println("|________________________________|");
            System.out.println("|Podaj nowe koszty lub wybierz 0 |");
            System.out.println("|________________________________|");
            dec = scanner.nextDouble();
            if(dec != 0){
                Save save = new Save();
                save.setStaticCosts(dec);
                System.out.print("\033[H\033[2J");
                System.out.flush();
                this.showMenu();
            }else{
                this.options();
            }
        }else if(decision.equals("2")) {
            System.out.println("|________________________________|");
            System.out.println("| Budżet oszczędnościowy wynosi: |");
            System.out.println("| "+data.getCurrentSavings() + " |");
            System.out.println("|________________________________|");
            System.out.println("|Podaj nowy budżet lub wybierz 0 |");
            System.out.println("|________________________________|");
            dec = scanner.nextDouble();
            if(dec != 0){
                Save save = new Save();
                save.setCurrentSavings(dec);
                System.out.print("\033[H\033[2J");
                System.out.flush();
                this.showMenu();
            }else{
                System.out.print("\033[H\033[2J");
                System.out.flush();
                this.options();
            }





        }else if(decision.equals("3")){

            System.out.println("|________________________________|");
            System.out.println("|   Budżet podróżniczy wynosi:   |");
            System.out.println("| "+data.getCurrentTravels() + " |");
            System.out.println("|________________________________|");
            System.out.println("|Podaj nowy budżet lub wybierz 0 |");
            System.out.println("|________________________________|");
            dec = scanner.nextDouble();
            if(dec != 0){
                Save save = new Save();
                save.setCurrentTravels(dec);
                System.out.print("\033[H\033[2J");
                System.out.flush();
                this.showMenu();
            }else {
                System.out.print("\033[H\033[2J");
                System.out.flush();
                this.options();
            }


        }
        else if(decision.equals("4")){
            System.out.print("\033[H\033[2J");
            System.out.flush();
            this.showMenu();
        }
        else{
            System.out.println("Opcja nie istnieje");
            this.showMenu();
        }



    }


}
