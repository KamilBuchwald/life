import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class Save extends Data {

    private JSONArray tempArray = new JSONArray();
    private double temdouble;

    public Save() throws JSONException {

    }


    private void saveData(JSONObject data) throws IOException {
        try (FileWriter file = new FileWriter("/home/devidea/Projects/workspace/Life/src/informations.json")) {
            file.write(data.toString());

        }
    }

    public void setStaticCosts(double costs) throws JSONException, IOException {

        JSONSource.getJSONArray("staticCosts").put(costs);
        this.saveData(JSONSource);



    }

    public void setCurrentSavings(double savings) throws JSONException, IOException {

        JSONSource.put("Savings", savings);
        this.saveData(JSONSource);

    }
    public void setCurrentTravels(double travels) throws IOException, JSONException {
        JSONSource.put("Travels", travels);
        this.saveData(JSONSource);

    }

    public void addGoal(JSONArray goal){

        /*Co do obiektu?
            - Nazwa
            - Koszty całkowite
            - Koszty:
                *Wydatki
                *Nocleg
                *Podróż

         */

    }
    public void addHolidayGoal(JSONObject holiday){

    }


    public void saveRevenues(JSONObject revenue) throws JSONException, IOException {




        JSONArray tmp = new JSONArray();
        tmp = JSONSource.getJSONArray("Revenues");
        tmp.put(revenue);
        JSONSource.put("Revenues", tmp);
        double newSavings = JSONSource.getDouble("Savings") + revenue.getDouble("savings");
        double newTravels = JSONSource.getDouble("Travels") + revenue.getDouble("travels");

        JSONSource.put("Savings", newSavings);
        JSONSource.put("Travels", newTravels);
        this.saveData(JSONSource);



    }


}
