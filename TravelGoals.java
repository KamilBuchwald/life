import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class TravelGoals extends Data {

    private JSONObject temObject;
    public JSONObject travelGoals, allGoals;

    private JSONArray temArray;
    private JSONArray tar;

    private String name;
    private double total;
    private int percent, priority;
    private boolean acces;


    public TravelGoals() throws JSONException {


    }

    public JSONObject calculateTravels() throws JSONException {


        JSONObject holidaysReportObject = new JSONObject();
        JSONObject holidaysReportGoals = new JSONObject();
        JSONArray holidaysReportArray = new JSONArray();

        for (int i = 0; i < travelsArray.length(); i++) {


            temObject = travelsArray.getJSONObject(i);
            name = temObject.getString("name");

            total = temObject.getDouble("totalPrice");
            if ((currentTravels / total) > 1) {
                percent = 100;
            } else {
                percent = (int) ((100 * currentTravels) / total);
            }

            if (percent >= 100) {
                acces = true;
            } else {
                acces = false;
            }


            holidaysReportGoals.put("Nazwa", name);
            holidaysReportGoals.put("Koszt całkowity", total);
            holidaysReportGoals.put("Procent dostępności", percent);
            holidaysReportGoals.put("Dostępność", acces);

            holidaysReportArray.put(holidaysReportGoals);
            holidaysReportObject.put("Holidays", holidaysReportArray);
            holidaysReportGoals = new JSONObject();


        }
        travelGoals = holidaysReportObject;

        return travelGoals;


    }

    public JSONObject calculateGoals() throws JSONException {


        JSONObject goalsReportObject = new JSONObject();
        JSONObject allGoalsReportGoals = new JSONObject();
        JSONArray goalsReportArray = new JSONArray();

        for (int i = 0; i < goalsArray.length(); i++) {


            temObject = goalsArray.getJSONObject(i);
            name = temObject.getString("name");

            total = temObject.getDouble("price");
            if ((currentTravels / total) > 1) {
                percent = 100;
            } else {
                percent = (int) ((100 * currentSavings) / total);
            }

            if (percent >= 100) {
                acces = true;
            } else {
                acces = false;
            }
            priority = temObject.getInt("priority");

            allGoalsReportGoals.put("Cel", name);
            allGoalsReportGoals.put("Koszt całkowity", total);
            allGoalsReportGoals.put("Procent dostępności", percent);
            allGoalsReportGoals.put("Priorytet", priority);

            goalsReportArray.put(allGoalsReportGoals);
            goalsReportObject.put("Cele", goalsReportArray);
            allGoalsReportGoals = new JSONObject();


        }
        allGoals = goalsReportObject;
        return allGoals;

    }


}



